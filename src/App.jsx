import { useEffect, useState } from 'react';

const App = () => {
  const [text, setText] = useState('');
  const [cat, setCat] = useState(null);
  const [darkMode, setDarkMode] = useState(false);

  const inputHandler = (e) => {
    setText(e.target.value);
  };

  const darkModeToggle = (e) => {
    setDarkMode(e.target.checked);
  };

  useEffect(() => {
    if (text === 'secret') alert('Wow Secret!');
  }, [text]);

  useEffect(() => {
    fetch('https://aws.random.cat/meow')
      .then((res) => res.json())
      .then((img) => setCat(img.file));
  }, []);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        backgroundColor: darkMode ? 'black' : 'white',
      }}
    >
      <div className='container'>
        <h1>Title</h1>
        <div>
          <input type='text' onChange={inputHandler} />
          <p>{text}</p>
          <label htmlFor='check'>Dark mode</label>
          <input type='checkbox' id='check' onChange={darkModeToggle} />
          <div>
            {cat ? (
              <img src={cat} alt='cat' className='cat-img' />
            ) : (
              <h1>loading..</h1>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
